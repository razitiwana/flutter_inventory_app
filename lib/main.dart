import 'package:flutter/material.dart';
import 'package:flutter_inventory_app/pages/HomePage.dart';

import 'package:flutter_inventory_app/managers/AuthManager.dart';

import 'pages/LoginPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(title: "Inventory App"),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Inventory App',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
//          textTheme: TextTheme()
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
