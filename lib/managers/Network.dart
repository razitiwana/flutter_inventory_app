import 'package:dio/dio.dart';

class Network {
  // Staging
  static final String _baseUrl = "http://admin.ashlarhq.com";
  static final String _client_secret =
      "RNNKkLUFYg8m5Kbmhvcd0g7P0TElD56BXfiSkYdH";
  static final int _client_id = 1;

  static var auth = "$_baseUrl/api/oauth/token";

  static Future<void> performLogin(String email, String password) async {
    Response response;
    Dio dio = new Dio();

    FormData formData = FormData.fromMap({
      "grant_type": "password",
      "client_secret": _client_secret,
      "client_id": _client_id,
      "username": email,
      "password": password
    });

    response = await dio.post(auth, data: formData);

    print(response.data);
  }
}
