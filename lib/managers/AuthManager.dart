import 'dart:convert';

import 'package:flutter_inventory_app/models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthManager {
  static User currentUser;

  static void setCurrentUser() {
    isUserLoggedIn().then((value) => {
          if (value)
            {
              getUser().then((user) => {currentUser = user})
            }
        });
  }

  static void loginUser(String token, User user) {
    _saveUser(user);
    _saveAuthToken(token);
  }

  static Future<void> _saveUser(User user) async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    Map decodeOptions = jsonDecode(userToJson(user));
    String userEncoded = jsonEncode(User.fromJson(decodeOptions));
    sharedUser.setString('user', userEncoded);
  }

  static Future<User> getUser() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    Map userMap = jsonDecode(sharedUser.getString('user'));
    var user = User.fromJson(userMap);
    return user;
  }

  static Future<bool> isUserLoggedIn() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    return (sharedUser.get('token') != null);
  }

  static Future<void> _saveAuthToken(String token) async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    sharedUser.setString('token', token);
  }

  static Future<String> getAuthToken() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    return sharedUser.get('token') ?? '';
  }
}
