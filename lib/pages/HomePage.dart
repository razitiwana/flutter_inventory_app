import 'package:flutter/material.dart';
import 'package:flutter_inventory_app/utils/HexColor.dart';

import 'CreateLoadPage.dart';
import 'LoadsListingPage.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  static String tag = 'home-page';

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  int _counter = 0;

  void _navigateToCreateLoads() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CreateLoadPage()),
    );
  }

  void _navigateToExistingLoadsListing() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoadListingPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                onPressed: () {
                  _navigateToCreateLoads();
                },
                color: HexColor('3171e0'),
                child: Text(
                  'Create Loads',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
            ),
            Expanded(
              child: FlatButton(
                color: HexColor('4c8dff'),
                textColor: Colors.white70,
                onPressed: () {
                  _navigateToExistingLoadsListing();
                },
                child: Text(
                  'View Existing Loads',
//                  style: TextStyle(color: Colors.white70),
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
