import 'package:flutter/material.dart';

class CreateLoadPage extends StatefulWidget {
  static String tag = 'createLoad-page';
  @override
  _CreateLoadPageState createState() => new _CreateLoadPageState();
}

class _CreateLoadPageState extends State<CreateLoadPage> {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Icon(
          Icons.airport_shuttle,
          color: Colors.deepPurple,
          size: 50.0,
        ),
      ),
    );

    final loadNumber = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Load #',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loadTitle = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Title',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final pallet = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Pallet #',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final createButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {},
        padding: EdgeInsets.all(12),
        color: Colors.deepPurple,
        child: Text('Create', style: TextStyle(color: Colors.white)),
      ),
    );

    final backtext = FlatButton(
      child: Text(
        'click here to go back',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            // Text('happy birthday login),
            logo,
            SizedBox(height: 48.0),
            loadNumber,
            SizedBox(height: 8.0),
            loadTitle,
            SizedBox(height: 8.0),
            pallet,
            SizedBox(height: 24.0),
            createButton,
            backtext
          ],
        ),
      ),
    );
  }
}
